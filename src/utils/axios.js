import Config from 'react-native-config'
import axios from 'axios'

async function searchNews(page, word){
    const req = await axios.get(`${Config.BASE_URL}/everything?q=${word}&page=${page}&apiKey=${Config.API_KEY}`)
    return req.data.articles
}

async function getNews(page){
    const req = await axios.get(`${Config.BASE_URL}/top-headlines?country=id&apiKey=${Config.API_KEY}&page=${page}`)
    return req.data.articles
}

export {searchNews, getNews}