import React, { Component } from 'react';
import { View, FlatList, TouchableHighlight, TouchableOpacity, TextInput } from 'react-native';
import { Text, Card, Divider, SearchBar } from 'react-native-elements';
import { styles } from './SearchNewsScreen-style.js';
import { searchNews, getNews } from '../../utils/axios'
import moment from 'moment';
import Icon from 'react-native-vector-icons/Ionicons';


class Search extends Component {
  state = {
    articles: [],
    refreshing: false,
    loading: true,
    url: '',
    page: 1,
    search: ''
  };


  handleNews(page, word) {
    searchNews(page, word).then(articles => {
      this.setState({ articles, refreshing: false })
      if (articles.length === 0) {
        alert('Please Search Another Word')
      }
    })
      .catch(() => this.setState({ refreshing: false }))
  }

  handleAddNews(page, word) {
    searchNews(page, word).then(articles => {
      let listData = this.state.articles
      if (articles.length === 0) {
        alert('End of result')
        this.setState({ articles: listData, refreshing: false })
        this.state.page = 1
      } else {
        let data = listData.concat(articles)
        this.setState({ articles: data, refreshing: false })
      }
    })
      .catch(() => this.setState({ refreshing: false }))
  }

  handleRefresh = () => {
    this.setState({ refreshing: true }, () => this.handleNews(this.state.page, this.state.search))
  }

  handleLoadMore = () => {
    this.state.page = this.state.page + 1
    this.setState({ refreshing: true }, () => this.handleAddNews(this.state.page, this.state.search))
  }

  WebViewScreen(url) {
    const { navigation } = this.props
    navigation.navigate('WebViewScreen', { url: url })
  }

  renderNews = ({ item }) => {
    const time = moment(item.publishedAt || moment.now()).fromNow();
    return (
      <TouchableOpacity useForeground onPress={() => {
        this.webViewScreen({ url: item.url })
      }}>
        <Card containerStyle={styles.cardNews}>
          <Card.Image containerStyle={styles.cardImage} source={{ uri: item.urlToImage }}>
            <Card.FeaturedTitle style={styles.featuredTitleStyle}>
              {item.title}
            </Card.FeaturedTitle>
          </Card.Image>

          <Text style={{ marginBottom: 10, marginTop: 10  }}>
            {item.description || 'Read More..'}
          </Text>
          <Divider style={{ backgroundColor: '#dfe6e9' }} />
          <View
            style={{ flexDirection: 'row', justifyContent: 'space-between' }}
          >
            <Text style={styles.noteStyle}>{item.author} | {item.source.name.toUpperCase()} </Text>
            <Text style={styles.noteStyle}>{time}</Text>
          </View>
        </Card>
      </TouchableOpacity>
    )
  }


  render(){
    console.log(this.state.articles)
    console.log(this.state.search)

    return(
        <View style={styles.bodyBackgroundColor}>
                <SearchBar inputStyle={styles.textInput}
                  placeholder="Search News Here"
                  lightTheme
                  placeholderTextColor="grey"
                  onChangeText={(search) => this.setState({ search })}
                  onSubmitEditing={() => this.handleRefresh()}
                  keyboardType='web-search'
                  value={this.state.search}
                  autoFocus={true}
                  color='black'
                  round
                />  

          <FlatList
            data={this.state.articles}
            renderItem={this.renderNews}
            keyExtractor={item => item.url}
            refreshing={this.state.refreshing}
            onRefresh={this.handleRefresh}
            onEndReachedThreshold={0.4}
            onEndReached={this.handleLoadMore}
          />
        </View>
    )
  }
}

export default Search;