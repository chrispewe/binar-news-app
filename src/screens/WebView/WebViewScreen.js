import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { WebView } from 'react-native-webview';


function WebViewScreen({route}) {
    const {url} = route.params
    return(
        <WebView source={{ uri: url.url }} />    
    )
}

export default WebViewScreen