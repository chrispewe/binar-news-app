import React, { Component } from 'react';
import { View, TouchableOpacity, FlatList } from 'react-native';
import { Text, Card, Divider, SearchBar } from 'react-native-elements';
import { SafeAreaView } from 'react-native-safe-area-context';
import { styles } from './NewsScreen-style.js';
import { searchNews, getNews } from '../../utils/axios';
import Icon from 'react-native-vector-icons/Ionicons';
import moment from 'moment';

class News extends Component {
  state = {
    articles: [],
    refreshing: true,
    loading: true,
    url: '',
    page: 1
  };

  componentDidMount = () => {
    this.handleNews();
  };

  handleNews = () => {
    getNews()
      .then(articles => {
        this.setState({ articles, refreshing: false });
      })
      .catch(() => this.setState({ refreshing: false }));
  };

  handleAddNews(page) {
    getNews(page).then(articles => {
      let listData = this.state.articles
      if (articles.length === 0) {
        alert('End of result')
        this.setState({ articles: listData, refreshing: false })
        this.state.page = 1
      } else {
        let data = listData.concat(articles)
        this.setState({ articles: data, refreshing: false })
      }
    })
      .catch(() => this.setState({ refreshing: false }))
  }

  handleLoadMore = () => {
    this.state.page = this.state.page + 1
    this.setState({ refreshing: true }, () => this.handleAddNews(this.state.page))
  }

  handleRefresh = () => {
    this.setState({ refreshing: true }, () => this.handleNews());
  };

  webViewScreen(url) {
    const { navigation } = this.props
    navigation.navigate('WebViewScreen', { url: url })
  }

  renderNews = ({ item }) => {
    const time = moment(item.publishedAt || moment.now()).fromNow();
    return (
      <TouchableOpacity useForeground onPress={() => {
        this.webViewScreen({ url: item.url })
      }}>
        <Card containerStyle={styles.cardNews}>
          <Card.Image containerStyle={styles.cardImage} source={{ uri: item.urlToImage }}>
            <Card.FeaturedTitle style={styles.featuredTitleStyle}>
              {item.title}
            </Card.FeaturedTitle>
          </Card.Image>

          <Text style={{ marginBottom: 10, marginTop: 10 }}>
            {item.description || 'Read More..'}
          </Text>
          <Divider style={{ backgroundColor: '#dfe6e9' }} />
          <View
            style={{ flexDirection: 'row', justifyContent: 'space-between' }}
          >
            <Text style={styles.noteStyle}>{item.author} | {item.source.name.toUpperCase()}</Text>
            <Text style={styles.noteStyle}>{time}</Text>
          </View>
        </Card>
      </TouchableOpacity>
    )
  }

  render() {
    const { navigation } = this.props
    console.log(this.state.articles);
    return (
      <SafeAreaView style={styles.bodyBackgroundColor}>
      <View style={styles.headerStyle}>
        <Text style={styles.headerText}>NEWS</Text>
        <TouchableOpacity>
        <Icon 
          name="search" 
          color="black" 
          size={40} 
          style={{marginRight:15, marginTop: 10}}
          onPress={() => {
            navigation.navigate('SearchNewsScreen')
          }}
        />
        </TouchableOpacity>
      </View> 
      <FlatList
        data={this.state.articles}
        renderItem={this.renderNews}
        keyExtractor={item => item.url}
        refreshing={this.state.refreshing}
        onRefresh={this.handleRefresh}
        onEndReachedThreshold={0.4}
        onEndReached={this.handleLoadMore}
      />
      </SafeAreaView>
    );
  }
}

export default News;
