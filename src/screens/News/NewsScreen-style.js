import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
    noteStyle: {
        margin: 5,
        fontStyle: 'italic',
        color: '#b2bec3',
        fontSize: 10
    },
    headerStyle:{
        backgroundColor: '#e4e6eb',
        flexDirection: 'row', 
        justifyContent: 'space-between'
    },
    headerText:{
        fontSize: 50,
        fontWeight: "bold",
        marginLeft: 15
    },
    featuredTitleStyle: {
        marginVertical: 20,
        marginHorizontal: 20,
        textShadowColor: '#00000f',
        textShadowOffset: { width: 3, height: 3 },
        textShadowRadius: 3
    },
    bodyBackgroundColor: {
        backgroundColor: '#FFFFFF',
        flex: 1
    },
    cardNews: {
        backgroundColor: '#e4e6eb',
        borderRadius: 16
    },
    cardImage: {
        borderRadius: 16
    }

})

export { styles }